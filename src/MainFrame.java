import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.CardLayout;
import java.awt.Component;
import java.io.FileNotFoundException;
import java.io.IOException;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JPanel curPanel;
	// private MazePanel mazePanel;
	// these should probably not be global. TODO - find a way to access labels
	// outside of the function in which they are created
	private Settings settings;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public MainFrame() throws FileNotFoundException, IOException {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(50, 50, 800, 600);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new CardLayout(0, 0));
		this.setContentPane(contentPane);

		this.settings = new Settings();

		// MENU PANEL
		MenuScreenPanel menuScreenPanel = new MenuScreenPanel(this);
		contentPane.add(menuScreenPanel, "menu");

		// MAZE SETTINGS PANEL
		GameSettingsPanel gameSettingsPanel = new GameSettingsPanel(this);
		contentPane.add(gameSettingsPanel, "mazeSettings");
		repaint();

	}

	public Settings getSettings() {
		return settings;
	}

	public void showCard(String cardName) {
		CardLayout cardLayout = (CardLayout) contentPane.getLayout();
		cardLayout.show(contentPane, cardName);
		Component toSet = null;
		for (Component c : contentPane.getComponents()){
			if (c.isVisible()) {
				curPanel = (JPanel) c;
			}
		}
		curPanel.requestFocus();
		curPanel.setFocusable(true);
		curPanel.requestFocusInWindow();
	}

	public void setFocus(String cardName) {
		Component curPanel = null;
		for (Component c : contentPane.getComponents()){
			if (c.isVisible()) {
				curPanel = (JPanel) c;
			}
		}
		curPanel.requestFocus();
		curPanel.setFocusable(true);
		curPanel.requestFocusInWindow();
	}
	
	public void addScreen(JPanel screen, String name) {
		this.contentPane.add(screen, name);
	}
}