import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

public class Maze {
	public static final int UP = 0;
	public static final int RIGHT = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	public static int MGX = 0;
	public static int MGY = 0;
	public static Pair LPATH;
	

	NodeGrid nodeGrid;
	int width;
	int height;
	ArrayList<Integer> freeDirection;
	Stack<Integer> directionHistory;
	
	public Maze(int width, int height) {
		if (width == 0)
			width = 1;
		if (height == 0)
			height = 1;
		this.width = width;
		this.height = height;
		nodeGrid = new NodeGrid(width, height);
		freeDirection = new ArrayList<Integer>();
		directionHistory = new Stack<Integer>();
		initialiseMaze();
	}

	/**
	 *  initialise the nodes as a fully filled in maze
	 */
	private void initialiseMaze() {
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < width; x++) {
				int up = Node.REMOVABLE_WALL;
				int down = Node.REMOVABLE_WALL;
				int left = Node.REMOVABLE_WALL;
				int right = Node.REMOVABLE_WALL;

				if (x == 0)
					left = Node.FIXED_WALL;
				if (x == width - 1)
					right = Node.FIXED_WALL;
				if (y == 0)
					up = Node.FIXED_WALL;
				if (y == getHeight() - 1)
					down = Node.FIXED_WALL;

				nodeGrid.put(new Node(up, down, left, right), x, y);
			}
		}
	}

	
	
	public void generateForestMaze(String mazeMode) {
		Random r = new Random();
		MGX = r.nextInt(width/2);
		MGY = r.nextInt(height);
		ArrayList<Pair> coordList = new ArrayList<Pair>();
		coordList.add(new Pair(MGX, MGY));
		/*
		String treeMode;
		treeMode = "random";
		treeMode = "recent";
		treeMode = "longWindy";
		 */
		
		int coordIndex = 0;
		int direction = -1;
		Node cur = nodeGrid.get(MGX, MGY);

		while (!allMazeSeen()) {
			if (mazeMode == "random")
				coordIndex = r.nextInt(coordList.size());
			if (mazeMode == "recent")
				coordIndex = coordList.size() - 1;
			if (mazeMode == "longWindy") {
				if (r.nextInt(4) < 1)
					coordIndex = r.nextInt(coordList.size());
				else
					coordIndex = coordList.size() - 1;
			} else {
				coordIndex = r.nextInt(coordList.size());
			}
			MGX = coordList.get(coordIndex).getX();
			MGY = coordList.get(coordIndex).getY();
			cur = nodeGrid.get(MGX, MGY);

			cur.setIsExplored(true);

			fillFreeDirection(cur);

			if (freeDirection.size() > 0) {
				int index = r.nextInt(freeDirection.size()) % 4;
				direction = (freeDirection.get(index));
			} else {
				direction = -1;
			}

			if (direction == -1) {
				Pair toRemove = null;
				for (Pair p : coordList) {
					if (p.getX() == MGX && p.getY() == MGY)
						toRemove = p;
				}
				coordList.remove(toRemove);

			} else {
				removeWall(direction, true);
				coordList.add(new Pair(MGX, MGY));
				cur = nodeGrid.get(MGX, MGY);
				cur.setIsExplored(true);
			}

		}

	}

	
	
	/**
	 * Remove a wall in the maze - used in DFS maze creation
	 * @param x the x-coord of the node to remove the wall from
	 * @param y the y-coord of the node to remove the wall from
	 * @param direction which wall to remove
	 */
	private void fillFreeDirection(Node cur) {
		freeDirection.clear();
		if (cur.getUp() != Node.FIXED_WALL) {
			if (!nodeGrid.up(MGX, MGY).getIsExplored())
				freeDirection.add(UP);
		}
		if (cur.getRight() != Node.FIXED_WALL) {
			if (!nodeGrid.right(MGX, MGY).getIsExplored())
				freeDirection.add(RIGHT);
		}
		if (cur.getDown() != Node.FIXED_WALL) {
			if (!nodeGrid.down(MGX, MGY).getIsExplored())
				freeDirection.add(DOWN);
		}
		if (cur.getLeft() != Node.FIXED_WALL) {
			if (!nodeGrid.left(MGX, MGY).getIsExplored())
				freeDirection.add(LEFT);
		}
	}

	private void travel(int direction, int numSteps, boolean traceback) {
		int counter = 0;
		while (counter < numSteps) {
			directionHistory.add(direction);
			if (direction == UP) {
				MGY--;
			} else if (direction == RIGHT) {
				MGX++;
			} else if (direction == DOWN) {
				MGY++;
			} else if (direction == LEFT) {
				MGX--;
			}
			counter++;
		}
	}

	private void removeWall(int direction, boolean travelToo) {
		if (direction == UP) {
			nodeGrid.get(MGX, MGY).setUp(Node.NO_WALL);
			Node n = nodeGrid.up(MGX, MGY);
			if (n != null)
				n.setDown(Node.NO_WALL);
			if (travelToo)
				travel(direction, 1, true);
		} else if (direction == RIGHT) {
			nodeGrid.get(MGX, MGY).setRight(Node.NO_WALL);
			Node n = nodeGrid.right(MGX, MGY);
			if (n != null)
				n.setLeft(Node.NO_WALL);
			if (travelToo)
				travel(direction, 1, true);
		} else if (direction == DOWN) {
			nodeGrid.get(MGX, MGY).setDown(Node.NO_WALL);
			Node n = nodeGrid.down(MGX, MGY);
			if (n != null)
				n.setUp(Node.NO_WALL);
			if (travelToo)
				travel(direction, 1, true);
		} else if (direction == LEFT) {
			nodeGrid.get(MGX, MGY).setLeft(Node.NO_WALL);
			Node n = nodeGrid.left(MGX, MGY);
			if (n != null)
				n.setRight(Node.NO_WALL);
			if (travelToo)
				travel(direction, 1, true);
		}
	}

	private void reverse() {
		int stackDir = directionHistory.pop();
		if (stackDir == UP) {
			MGY++;
		} else if (stackDir == RIGHT) {
			MGX--;
		} else if (stackDir == DOWN) {
			MGY--;
		} else if (stackDir == LEFT) {
			MGX++;
		}
	}

	public boolean allMazeSeen() {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (nodeGrid.get(x, y).getIsExplored() == false) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * A function to print a maze using '|', '_' and ' ' ASCII characters
	 */
	public void printMaze() {
		System.out.print("_");
		for (int i = 0; i < width; i++) {
			System.out.print("__"); // top row has no gaps
		}
		System.out.println("");
		for (int i = 0; i < height; i++) {
			System.out.print("|"); // left column has no gaps
			for (int j = 0; j < width; j++) {
				if (nodeGrid.get(j, i).getDown() != 0) { // top of lower square
															// included
					System.out.print("_");
				} else {
					System.out.print(" ");
				}
				if (nodeGrid.get(j, i).getRight() != 0) { // left of square to
															// right included
					System.out.print("|");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}
	}
	
	/**
	 * @param x the x-coord of the node to find
	 * @param y the y-coord of the node to find
	 * @return the node at those coordinates
	 */
	public Node getNodeAt(int x, int y) {
		return nodeGrid.get(x, y);
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}
}
