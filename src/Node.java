
public class Node {

	public static final int NO_WALL = 0;
	public static final int REMOVABLE_WALL = 1;
	public static final int FIXED_WALL = 2;

	// Either No, Removable or Fixed wall
	private int up;
	private int down;
	private int left;
	private int right;
	
	private boolean isExplored;
	
	public Node(int up, int down, int left, int right) {
		this.up = up;
		this.down = down;
		this.left = left;
		this.right = right;
	}
	
	/**
	 * @return the wall to that side - from NO_WALL to FIXED_WALL
	 */
	public int getUp() {
		return this.up;
	}
	
	public int getDown() {
		return this.down;
	}
	
	public int getLeft() {
		return this.left;
	}
	
	public int getRight() {
		return this.right;
	}
	
	/**
	 * @return whether this Node has been checked in DFS maze creation yet
	 */
	public boolean getIsExplored() {
		return isExplored;
	}
	
	/**
	 * Sets the state of the wall in that direction
	 * @param up/down/left/right the new state of the wall - from NO_WALL to FIXED_WALL
	 */
	public void setUp(int up) {
		this.up = up;
	}
	
	public void setDown(int down) {
		this.down = down;
	}
	
	public void setLeft(int left) {
		this.left = left;
	}
	
	public void setRight(int right) {
		this.right = right;
	}
	
	/**
	 * Sets whether this Node has been passed through by the maze creation DFS algorithm
	 * @param isExplored whether the Node has been seen or not
	 */
	public void setIsExplored(boolean isExplored) {
		this.isExplored = isExplored;
	}
	
}
