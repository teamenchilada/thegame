import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

/**
 * 
 */

/**
 * @author Jens
 *
 */
public class GameSettingsPanel extends JPanel implements ActionListener {
	private MainFrame mf;
	private JButton btnSmall;
	private JButton btnMedium;
	private JButton btnLarge;
	private JButton btnCustom;
	private JButton btnGenerate;
	private JSpinner spnX;
	private JSpinner spnY;
	private int x;
	private int y;
	
	private Color selected;
	private Color unselected;
	
	public GameSettingsPanel(MainFrame mf) {
		this.x = 10;
		this.y = 10;
		this.mf = mf;
		this.setLayout(null);
		selected = new Color (100, 150, 200);
		unselected = this.mf.getSettings().getBackgroundColour();
		JLabel lblTitle = new JLabel("MAZE SIZE:");
		lblTitle.setFont(this.mf.getSettings().getFont());
		lblTitle.setOpaque(true);
		lblTitle.setBounds(300, 100, 100, 50);
		this.add(lblTitle);
		
		btnSmall = new JButton("SMALL");
		btnSmall.setFont(this.mf.getSettings().getFont());
		btnSmall.setOpaque(true);
		btnSmall.setBorderPainted(false);
		btnSmall.addActionListener(this);
		btnSmall.setBounds(300, 200, 100, 50);
		btnSmall.setForeground(mf.getSettings().getForegroundColour());
		this.add(btnSmall);
		
		btnMedium = new JButton("MEDIUM");
		btnMedium.setFont(this.mf.getSettings().getFont());
		btnMedium.setOpaque(true);
		btnMedium.setBorderPainted(false);
		btnMedium.addActionListener(this);
		btnMedium.setBounds(300, 300, 100, 50);
		btnMedium.setForeground(mf.getSettings().getForegroundColour());
		this.add(btnMedium);
		
		btnLarge = new JButton("LARGE");
		btnLarge.setFont(this.mf.getSettings().getFont());
		btnLarge.setOpaque(true);
		btnLarge.setBorderPainted(false);
		btnLarge.addActionListener(this);
		btnLarge.setBounds(300, 400, 100, 50);
		btnLarge.setForeground(mf.getSettings().getForegroundColour());
		this.add(btnLarge);
		
		btnCustom = new JButton("CUSTOM");
		btnCustom.setFont(this.mf.getSettings().getFont());
		btnCustom.setOpaque(true);
		btnCustom.setBorderPainted(false);
		btnCustom.addActionListener(this);
		btnCustom.setBounds(300, 500, 100, 50);
		btnCustom.setForeground(mf.getSettings().getForegroundColour());
		this.add(btnCustom);
		
		JLabel lblX = new JLabel("CUSTOM X:");
		lblX.setFont(this.mf.getSettings().getFont());
		lblX.setOpaque(true);
		lblX.setBounds(410, 500, 100, 25);
		this.add(lblX);
		
		JLabel lblY = new JLabel("CUSTOM Y:");
		lblY.setFont(this.mf.getSettings().getFont());
		lblY.setOpaque(true);
		lblY.setBounds(410, 525, 100, 25);
		this.add(lblY);
		
		SpinnerModel modelX = new SpinnerNumberModel(10, 1, 40, 1);
		spnX = new JSpinner(modelX);
		spnX.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				System.out.println("Spinner pressed");
				btnSmall.setBackground(unselected);
				btnMedium.setBackground(unselected);
				btnLarge.setBackground(unselected);
				btnCustom.setBackground(selected);
			}
		});
		spnX.setFont(this.mf.getSettings().getFont());
		spnX.setOpaque(true);
		spnX.setBounds(510, 500, 100, 25);
		this.add(spnX);
		
		SpinnerModel modelY = new SpinnerNumberModel(10, 1, 40, 1);
		spnY = new JSpinner(modelY);
		spnY.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				System.out.println("Spinner pressed");
				btnSmall.setBackground(unselected);
				btnMedium.setBackground(unselected);
				btnLarge.setBackground(unselected);
				btnCustom.setBackground(selected);
			}
		});
		spnY.setFont(this.mf.getSettings().getFont());
		spnY.setOpaque(true);
		spnY.setBounds(510, 525, 100, 25);
		this.add(spnY);
		
		btnSmall.setBackground(unselected);
		btnMedium.setBackground(unselected);
		btnLarge.setBackground(unselected);
		btnCustom.setBackground(unselected);
		
		btnGenerate = new JButton("Start");
		btnGenerate.setFont(this.mf.getSettings().getFont());
		btnGenerate.setOpaque(true);
		btnGenerate.setBorderPainted(false);
		btnGenerate.addActionListener(this);
		btnGenerate.setBounds(300, 5, 100, 50);
		btnGenerate.setForeground(mf.getSettings().getForegroundColour());
		this.add(btnGenerate);
		
		// By default have 'small' selected
		x = 10;
		y = 10;
		btnSmall.setBackground(selected);
		btnMedium.setBackground(unselected);
		btnLarge.setBackground(unselected);
		btnCustom.setBackground(unselected);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		MazeScreenPanel mazeScreenPanel;
		String mazeMode = "random";
		/*
		 * longWindy
		 * recent 
		 * random
		 * DFS
		 */
		if (e.getSource() == btnSmall) {
			x = 10;
			y = 10;
			btnSmall.setBackground(selected);
			btnMedium.setBackground(unselected);
			btnLarge.setBackground(unselected);
			btnCustom.setBackground(unselected);
		} else if (e.getSource() == btnMedium) {
			x = 18;
			y = 18;	
			btnSmall.setBackground(unselected);
			btnMedium.setBackground(selected);
			btnLarge.setBackground(unselected);
			btnCustom.setBackground(unselected);
		} else if (e.getSource() == btnLarge) {
			x = 24;
			y = 24;	
			btnSmall.setBackground(unselected);
			btnMedium.setBackground(unselected);
			btnLarge.setBackground(selected);
			btnCustom.setBackground(unselected);
		} else if (e.getSource() == btnCustom) {
			//x = (int) spnX.getValue();	// unnecessary because of check in start button
			//y = (int) spnY.getValue();
			btnSmall.setBackground(unselected);
			btnMedium.setBackground(unselected);
			btnLarge.setBackground(unselected);
			btnCustom.setBackground(selected);
		} else if (e.getSource() == btnGenerate) {
			if (btnCustom.getBackground().equals(selected)) {
				x = (Integer) spnX.getValue();
				y = (Integer) spnY.getValue();	// fixes potential inconsistencies
			}
			mazeScreenPanel = new MazeScreenPanel(mf, x, y, mazeMode);
			mf.addScreen(mazeScreenPanel, "maze");
			
			mazeScreenPanel.setLayout(null);
			mazeScreenPanel.setFocusable(true);
			mazeScreenPanel.requestFocusInWindow();
			mf.showCard("maze");
		}
	}
}
