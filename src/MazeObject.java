
public class MazeObject {

	private int x;
	private int y;

	// TODO
	// Fill this in as necessary
	
	public MazeObject(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * @return the object's x-coordinate
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * @return the object's y-coordinate
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the new position of the object
	 * @param x the new x-coordinate
	 * @param y the new y-coordinate
	 */
	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
}
