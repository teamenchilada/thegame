
// Grid class to store internal grid of nodes
public class NodeGrid {

	private int width;
	private int height;
	
	private Node[][] data;
	
	public NodeGrid(int width, int height) {
		this.width = width;
		this.height = height;
		data = new Node[width][height];
	}
	
	/**
	 * @return the width of the grid
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * @return the height of the grid
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Get object at (x, y)
	 * @param x the x-coord of the object to find
	 * @param y the y-coord of the object to find
	 * @return the object at those coordinates
	 */
	public Node get(int x, int y) {
		if (x < 0) x = 0;
		if (y < 0) y = 0;
		return data[x][y];
	}

	/**
	 * Put object at (x, y)
	 * @param obj the object to be inserted
	 * @param x the x-coord to insert the object at
	 * @param y the y-coord to insert the object at
	 */
	public void put(Node obj, int x, int y) {
		data[x][y] = obj;
	}

	/**
	 * Puts at coordinates with offset (up, right)
	 * Accepts negative up, right
	 * @param obj the object to be inserted
	 * @param x the x-coord to insert the object at
	 * @param y the y-coord to insert the object at
	 * @param up the vertical offset (can be negative)
	 * @param right the horizontal offset (can be negative)
	 */
	public void put(Node obj, int x, int y, int up, int right) {
		data[x + up][y + right] = obj;
	}

	/**
	 * Gets the object directly next to coordinates (null if invalid)
	 * @param x the x-coord of the object to find
	 * @param y the y-coord of the object to find
	 * @return the object in the appropriate direction
	 */
	public Node up(int x, int y) {
		return up(x, y, 1);
	}

	public Node down(int x, int y) {
		return down(x, y, 1);
	}

	public Node left(int x, int y) {
		return left(x, y, 1);
	}

	public Node right(int x, int y) {
		return right(x, y, 1);
	}

	/**
	 * Gets the object n places away from coordinates (null if invalid)
	 * Accepts negative n
	 * @param x the x-coord of the object to find
	 * @param y the y-coord of the object to find
	 * @param n the offset in the appropriate direction
	 * @return the object in the appropriate direction
	 */
	public Node up(int x, int y, int n) {
		if (y - n >= 0) {
			return get(x, y - n);
		} else {
			return null;
		}
	}

	public Node down(int x, int y, int n) {
		if (y + n < height) {
			return get(x, y + n);
		} else {
			return null;
		}
	}

	public Node left(int x, int y, int n) {
		if (x - n >= 0) {
			return get(x - n, y);
		} else {
			return null;
		}
	}

	public Node right(int x, int y, int n) {
		if (x + n < width) {
			return get(x + n, y);
		} else {
			return null;
		}
	}
	
}
