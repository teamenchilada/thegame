import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MenuScreenPanel extends JPanel implements ActionListener {
	private String bgUrl;
	private BufferedImage bgImage;
	private MainFrame mf;
	private JButton btnPlay;
	
	//TODO convert objects to private fields.
	public MenuScreenPanel(MainFrame mf) {
		this.mf = mf;
		bgUrl = "images/menu.jpg";
		setBackground();
		this.setLayout(null);
	
		// PLAY BUTTON
		btnPlay = new JButton("PLAY");
		//TODO Play button image
		
		btnPlay.setOpaque(false);
		// btnPlay.setContentAreaFilled(false);
		btnPlay.setBorderPainted(false);
		btnPlay.setBounds(316, 181, 108, 59);
		btnPlay.addActionListener(this);

		// HOVER
		/*
		 * btnPlay.addMouseListener(new java.awt.event.MouseAdapter() { public
		 * void mouseEntered(java.awt.event.MouseEvent evt) {
		 * //System.out.println("MOUSE HOVER"); Image img2 = new
		 * ImageIcon(img).getImage() ; Image newimg = img2.getScaledInstance(
		 * 200, 70, java.awt.Image.SCALE_SMOOTH ) ; btnPlay.setIcon(new
		 * ImageIcon(newimg)); btnPlay.setBounds(272, 50, 200, 70); }
		 * 
		 * public void mouseExited(java.awt.event.MouseEvent evt) {
		 * //System.out.println("MOUSE EXIT");
		 * 
		 * Image img3 = new ImageIcon(img).getImage() ; Image newimg =
		 * img3.getScaledInstance( 108, 59, java.awt.Image.SCALE_SMOOTH ) ;
		 * btnPlay.setIcon(new ImageIcon(newimg)); btnPlay.setBounds(272, 50,
		 * 108, 59); } });
		 */

		this.add(btnPlay);

		JLabel titleLabel = new JLabel("The Happy Maze", SwingConstants.CENTER);
		titleLabel.setBounds(0, 36, 289, 130);
		titleLabel.setFont(this.mf.getSettings().getFont());

		this.add(titleLabel); 
	}

	private void setBackground(){
		try {
			bgImage = ImageIO.read(new FileInputStream(bgUrl));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bgImage, 0, 0, null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.btnPlay)) {
			mf.showCard("mazeSettings");
		}
	}
}
