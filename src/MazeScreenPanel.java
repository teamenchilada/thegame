import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MazeScreenPanel extends JPanel implements KeyListener{
	public final static int UP_KEY = KeyEvent.VK_UP;
	public final static int DOWN_KEY = KeyEvent.VK_DOWN;
	public final static int LEFT_KEY = KeyEvent.VK_LEFT;
	public final static int RIGHT_KEY = KeyEvent.VK_RIGHT;

	public final static int W_KEY = KeyEvent.VK_W;
	public final static int S_KEY = KeyEvent.VK_S;
	public final static int A_KEY = KeyEvent.VK_A;
	public final static int D_KEY = KeyEvent.VK_D;
	
	private MainFrame mf;
	private MazePanel mazePanel;
	private TopMenuPanel topMenuPanel;
	
	public MazeScreenPanel(MainFrame mf, int x, int y, String mazeMode) {
		this.mf = mf;
		mazePanel = new MazePanel(x, y, mazeMode);
		topMenuPanel = new TopMenuPanel(this.mf);
		topMenuPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		topMenuPanel.setBounds(0, 0, 430, 90);
		
		mazePanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		mazePanel.setBounds(0, 95, 800, 430);
//		mf.add(this);
		this.add(topMenuPanel);
		this.add(mazePanel);
		this.addKeyListener(this);
		this.setFocusTraversalKeysEnabled(true);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0.getKeyCode());
		switch(arg0.getKeyCode()) {
		case UP_KEY:
			System.out.println("Move up");
			mazePanel.tryMoveUp(mazePanel.getPlayer1());
			repaint();
			break;
		case DOWN_KEY:
			System.out.println("Move down");
			mazePanel.tryMoveDown(mazePanel.getPlayer1());
			repaint();
			break;
		case LEFT_KEY:
			System.out.println("Move left");
			mazePanel.tryMoveLeft(mazePanel.getPlayer1());
			repaint();
			break;
		case RIGHT_KEY:
			System.out.println("Move right");
			mazePanel.tryMoveRight(mazePanel.getPlayer1());
			repaint();
			break;
		case W_KEY:
			System.out.println("2 Move up");
			mazePanel.tryMoveUp(mazePanel.getPlayer2());
			repaint();
			break;
		case S_KEY:
			System.out.println("2 Move down");
			mazePanel.tryMoveDown(mazePanel.getPlayer2());
			repaint();
			break;
		case A_KEY:
			System.out.println("2 Move left");
			mazePanel.tryMoveLeft(mazePanel.getPlayer2());
			repaint();
			break;
		case D_KEY:
			System.out.println("2 Move right");
			mazePanel.tryMoveRight(mazePanel.getPlayer2());
			repaint();
			break;
		}
		mazePanel.checkIfFinished();
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
