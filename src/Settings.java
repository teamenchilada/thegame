import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;

/**
 * @author Jens
 *
 */
public class Settings {
	private Font font;
	private Color backgroundColour;
	private Color foregroundColour;
	
	public Settings() {
		font = null;
		try
		{
		    font = Font.createFont(Font.TRUETYPE_FONT, new File("font/absender1.ttf"));
		    font = font.deriveFont(18f);

		    GraphicsEnvironment ge = 
		        GraphicsEnvironment.getLocalGraphicsEnvironment();
		    ge.registerFont(font);
		}
		catch ( FontFormatException ex ){ex.printStackTrace();}
		catch ( IOException ex ){ex.printStackTrace();}
		
		backgroundColour = Color.ORANGE;	// replace with whatever standard colours are
		foregroundColour = Color.BLACK;
	}
	
	/**
	 * @return the font
	 */
	public Font getFont() {
		return this.font;
	}
	
	/**
	 * @param font the new font
	 */
	public void setFont(Font font) {
		this.font = font;
	}
	
	/**
	 * @return the backgroundColour
	 */
	public Color getBackgroundColour() {
		return this.backgroundColour;
	}
	
	/**
	 * @param backgroundColour the new background colour
	 */
	public void setBackgroundColour(Color backgroundColour) {
		this.backgroundColour = backgroundColour;
	}
	
	/**
	 * @return the foregroundColour
	 */
	public Color getForegroundColour() {
		return this.foregroundColour;
	}
	
	/**
	 * @param foregroundColour the new colour
	 */
	public void setHighlightColour(Color foregroundColour) {
		this.foregroundColour = foregroundColour;
	}
	
	
}
