import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class MazePanel extends JPanel{

	public final static int UP_KEY = KeyEvent.VK_UP;
	public final static int DOWN_KEY = KeyEvent.VK_DOWN;
	public final static int LEFT_KEY = KeyEvent.VK_LEFT;
	public final static int RIGHT_KEY = KeyEvent.VK_RIGHT;

	public final static int W_KEY = KeyEvent.VK_W;
	public final static int S_KEY = KeyEvent.VK_S;
	public final static int A_KEY = KeyEvent.VK_A;
	public final static int D_KEY = KeyEvent.VK_D;
	
	// This should probably not be global
	private Maze m;
	private Character player1;
	private Character player2;
	private EndPoint endPoint;
	
	private int width;
	private int height;
	private String mazeMode;
	

	public Character getPlayer1() {
		return player1;
	}

	public Character getPlayer2() {
		return player2;
	}

	/**
	 * Create the panel.
	 */
	public MazePanel(int x, int y, String mazeMode) {
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);
		this.width = x;
		this.height = y;
		this.mazeMode = mazeMode;
		m = new Maze(x, y);
		m.generateForestMaze(mazeMode);
		player1 = new Character(0, 0);
		player2 = new Character(x-1, 0);
		endPoint = new EndPoint(x-1, y-1);
	}
	
	/**	
	 * Draw the maze
	 * @param g2d the graphics module
	 */
	private void paintMaze(Graphics2D g2d) {
		// Get the cell size in pixels
		int cellSize = getCellSize(this.m.getWidth(), this.m.getHeight());
		
		// Iterate over each maze node, and display the cell
		for (int x = 0; x < this.m.getWidth(); x++) {
			for (int y = 0; y < this.m.getHeight(); y++) {
				Node curNode = this.m.getNodeAt(x, y);
				drawCell(curNode, cellSize, x, y, g2d);
			}
		}
	}
	
	/**	
	 * Draw the players
	 * @param g2d the graphics module
	 */
	private void paintCharacter(Graphics2D g2d) {
		// Get the cell size in pixels
		int cellSize = getCellSize(this.m.getWidth(), this.m.getHeight());
		int dispX = player1.getX() * cellSize;
		int dispY = player1.getY() * cellSize;
		
		g2d.setColor(Color.BLACK);
		g2d.fillOval(dispX, dispY, cellSize, cellSize);
		
		dispX = player2.getX() * cellSize;
		dispY = player2.getY() * cellSize;
		
		g2d.setColor(Color.CYAN);
		g2d.fillOval(dispX, dispY, cellSize, cellSize);
	}
	
	/**	
	 * Draw the endpoint
	 * @param g2d the graphics module
	 */
	private void paintEndpoint(Graphics2D g2d) {
		// Get the cell size in pixels
		int cellSize = getCellSize(this.m.getWidth(), this.m.getHeight());
		int dispX = endPoint.getX() * cellSize;
		int dispY = endPoint.getY() * cellSize;
		
		g2d.setColor(Color.GRAY);
		g2d.fillRoundRect(dispX, dispY, cellSize, cellSize, dispX / 20, dispX / 20);
	}
	
	/**
	 * Draw a cell.
	 * @param n the node to be drawn
	 * @param cellSize the width of the node
	 * @param x the x-coord in the panel to draw the node
	 * @param y the y-coord in the panel to draw the node
	 */
	private void drawCell(Node n, int cellSize, int x, int y, Graphics2D g2d) {
		g2d.setColor(Color.BLACK);
		
		// The top-left coordinates of the cell in gui
		int dispX = x * cellSize;
		int dispY = y * cellSize;
		
		if (n.getUp() != Node.NO_WALL) {
			// Draw up wall
			g2d.drawLine(dispX, dispY, dispX + cellSize - 1, dispY);
		}
		if (n.getDown() != Node.NO_WALL) {
			// Draw down wall
			g2d.drawLine(dispX, dispY + cellSize - 1, dispX + cellSize - 1, dispY + cellSize - 1);
		}
		if (n.getLeft() != Node.NO_WALL) {
			// Draw left wall
			g2d.drawLine(dispX, dispY, dispX, dispY + cellSize - 1);
		}
		if (n.getRight() != Node.NO_WALL) {
			// Draw right wall
			g2d.drawLine(dispX + cellSize - 1, dispY, dispX + cellSize - 1, dispY + cellSize - 1);
		}
	}
	
	/**
	 * Calculate the best cell size.
	 * @param nCellsHor the horizontal number of Nodes in the Maze
	 * @param nCellsVer the vertical number of Nodes in the Maze
	 * @return the size each Node should be
	 */
	private int getCellSize(int nCellsHor, int nCellsVer) {
		return Math.min(getWidth() / nCellsHor, getHeight() / nCellsVer);
	}

	/**
	 * paints the screen
	 */
	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		
		// Make the background white
		//g2d.setColor(Color.ORANGE);
		//g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.clearRect(0, 0, getWidth(), getHeight());
		paintMaze(g2d);
		paintCharacter(g2d);
		paintEndpoint(g2d);
	}

	
	
	/**
	 * Moves the player in the specified direction.
	 * No movement is completed if there is a wall in the way.
	 */
	public void tryMoveUp(Character player) {
		int playerX = player.getX();
		int playerY = player.getY();
		if (playerY - 1 >= 0 && m.getNodeAt(playerX, playerY).getUp() == Node.NO_WALL)
			player.setPos(playerX, playerY - 1);
	}
	
	public void tryMoveDown(Character player) {
		int playerX = player.getX();
		int playerY = player.getY();
		if (playerY + 1 < m.getHeight() && m.getNodeAt(playerX, playerY).getDown() == Node.NO_WALL)
			player.setPos(playerX, playerY + 1);
	}
	
	public void tryMoveLeft(Character player) {
		int playerX = player.getX();
		int playerY = player.getY();
		if (playerX - 1 >= 0 && m.getNodeAt(playerX, playerY).getLeft() == Node.NO_WALL)
			player.setPos(playerX - 1, playerY);
	}
	
	public void tryMoveRight(Character player) {
		int playerX = player.getX();
		int playerY = player.getY();
		if (playerX + 1 < m.getWidth() && m.getNodeAt(playerX, playerY).getRight() == Node.NO_WALL)
			player.setPos(playerX + 1, playerY);
	}
	
	private boolean hasCollision(MazeObject o1, MazeObject o2) {
		return o1.getX() == o2.getX() && o1.getY() == o2.getY();
	}

	public void checkIfFinished() {
		if (hasCollision(player1, endPoint)) {
			System.out.println("Player 1 wins");
			newGame();
		} else if (hasCollision(player2, endPoint)) {
			System.out.println("Player 2 wins");
			newGame();
		}
	}

	public void newGame() {
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(null);
		m = new Maze(width, height);
		m.generateForestMaze(mazeMode);
		player1 = new Character(0, 0);
		player2 = new Character(width-1, 0);
		endPoint = new EndPoint(width-1, height-1);
	}

}
