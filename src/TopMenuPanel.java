import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class TopMenuPanel extends JPanel implements ActionListener {
	private MainFrame mf;
	private int timer;
	private JLabel lblTimer;
	private JButton btnMenu;
	
	public TopMenuPanel(MainFrame mf) {
		this.mf = mf;
		this.setLayout(null);
		
		//MENU BUTTON
		btnMenu = new JButton("MENU");
		//Image img0 = ImageIO.read(new FileInputStream("images/menu.png"));
		//btnNewButton.setIcon(new ImageIcon(img0));
		btnMenu.setFont(this.mf.getSettings().getFont());

		//btnNewButton.setForeground(mf.getSettings().getBackgroundColour());
		btnMenu.setOpaque(true);
		btnMenu.setBorderPainted(false);
		btnMenu.addActionListener(this);
		btnMenu.setBounds(0, 0, 88, 83);
		this.add(btnMenu);
		
		JButton btnPath = new JButton("PATH");
		//btnPath.setText("<html><body>PATH<br>ON</body></html>");
		//Image img2 = ImageIO.read(new FileInputStream("images/pathon.png"));
		//btnPath.setIcon(new ImageIcon(img2));
		btnPath.setFont(this.mf.getSettings().getFont());


		btnPath.setOpaque(true);
		btnPath.setBorderPainted(false);
		btnPath.setBounds(88, 0, 88, 83);
		this.add(btnPath);
		
		JButton btnTimer = new JButton("TIMER");
		//Image img3 = ImageIO.read(new FileInputStream("images/timer.png"));
		//btnTimer.setIcon(new ImageIcon(img3));
		btnTimer.setFont(this.mf.getSettings().getFont());

		btnTimer.setOpaque(true);
		btnTimer.setBorderPainted(false);
		btnTimer.setBounds(176, 0, 88, 52);
		this.add(btnTimer);
		
		JButton btnReset = new JButton("RESET");
		//Image img4 = ImageIO.read(new FileInputStream("images/reset.png"));
		//btnReset.setIcon(new ImageIcon(img4));
		btnReset.setFont(this.mf.getSettings().getFont());

		btnReset.setOpaque(true);
		btnReset.setBorderPainted(false);
		btnReset.setBounds(264, 0, 88, 83);
		this.add(btnReset);
		
		JButton btnSolve = new JButton("SOLVE");
		//Image img5 = ImageIO.read(new FileInputStream("images/solve.png"));
		//btnSolve.setIcon(new ImageIcon(img5));
		btnSolve.setFont(this.mf.getSettings().getFont());

		btnSolve.setOpaque(true);
		btnSolve.setBorderPainted(false);
		btnSolve.setBounds(352, 0, 88, 83);
		this.add(btnSolve);
		
		//BACKGROUND COLOR 
		btnMenu.setBackground(mf.getSettings().getBackgroundColour());
		btnPath.setBackground(mf.getSettings().getBackgroundColour());
		btnTimer.setBackground(mf.getSettings().getBackgroundColour());
		btnReset.setBackground(mf.getSettings().getBackgroundColour());
		btnSolve.setBackground(mf.getSettings().getBackgroundColour());

		
		lblTimer = new JLabel("0:00", JLabel.CENTER);
		lblTimer.setOpaque(true);
		lblTimer.setBackground(mf.getSettings().getBackgroundColour());
		lblTimer.setFont(mf.getSettings().getFont());
		lblTimer.setBounds(176, 50, 92, 33);
		
		this.add(lblTimer);
		
		Timer timer = new Timer(1000, this);
		timer.setInitialDelay(1000);
		timer.start();
	}

	/**
	 * Increases the timer whenever a second progresses.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.btnMenu)) {
			mf.showCard("menu");
		} else {
			timer += 1;
			if (timer % 100 > 59) {
				timer -= 60;
				timer += 100;
			}
			String toPrint = timer / 100 + ":";
			if (timer / 100 < 10) {
				toPrint += "0" + timer % 100;
			} else {
				toPrint += timer % 100;
			}
			lblTimer.setText(toPrint);
			repaint();
			mf.setFocus("maze");
		}
	}

}
